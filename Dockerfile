FROM docker.io/library/python:3.9.20-slim-bookworm

RUN apt-get update && apt-get install -y \
        jq \
        git && apt-get clean

COPY . /educationpruneeducations/
WORKDIR /educationpruneeducations

RUN pip install -r requirements.txt

CMD [ "python3", "/educationpruneeducations/docker_starter.py" ]
