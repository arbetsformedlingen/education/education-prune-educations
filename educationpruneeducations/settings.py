import os

ES_HOST = os.getenv("ES_HOST", "127.0.0.1").split(',')
ES_PORT = os.getenv("ES_PORT", 19200)
ES_USER = os.getenv("ES_USER", "admin")
ES_PWD = os.getenv("ES_PWD", "admin")
ES_USE_SSL = os.getenv("ES_USE_SSL", False)
ES_VERIFY_CERTS = os.getenv("ES_VERIFY_CERTS", False)
#
ES_EDUCATIONS_ALIAS = os.getenv('ES_EDUCATIONS_ALIAS', 'educations-merged-enriched')

PRUNE_EDUCATIONS_ES_INDEX_PATTERN = os.getenv('PRUNE_EDUCATIONS_ES_INDEX_PATTERN', 'educations-merged-enriched-20')
PRUNE_EDUCATIONS_ES_INDEX_AFTER_HOURS = int(os.getenv('PRUNE_EDUCATIONS_ES_INDEX_AFTER_HOURS', 24 * 3))

# Note: the file prefix 'educationplans_' have special treatment in the logic and should not be configured here.
PRUNE_EDUCATIONS_S3_FILE_PREFIX = os.getenv('PRUNE_EDUCATIONS_S3_FILE_PREFIX', 'educations_susanavet_;merged_educations_;merged_and_enriched_educations_')
PRUNE_EDUCATIONS_S3_FILE_AFTER_HOURS = int(os.getenv('PRUNE_EDUCATIONS_S3_FILE_AFTER_HOURS', 24 * 3))

S3_BUCKET_NAME = os.environ.get('S3_BUCKET_NAME', 'kll-test')
AWS_ACCESS_KEY = os.environ.get('AWS_ACCESS_KEY', 'kll')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY', 'abcd1234')
S3_URL = os.environ.get('S3_URL', 'http://localhost:19000')
