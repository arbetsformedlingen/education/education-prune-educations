
import logging

from opensearchpy import OpenSearch
from opensearchpy.client.cat import CatClient
from opensearchpy.client.indices import IndicesClient

from educationpruneeducations import settings

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

os_client = None

def get_os_client():
    global os_client
    if os_client:
        return os_client
    else:
        os_client = create_os_client()
    return os_client

def create_os_client():
    log.info("Using OpenSearch at %s:%s" % (settings.ES_HOST, settings.ES_PORT))

    auth = (settings.ES_USER, settings.ES_PWD)
    #ca_certs_path = '/full/path/to/root-ca.pem' # Provide a CA bundle if you use intermediate CAs with your root CA.

    new_os_client = OpenSearch(
        hosts = [{'host': host, 'port': settings.ES_PORT} for host in settings.ES_HOST],
        http_compress = True, # enables gzip compression for request bodies
        http_auth = auth,
        # client_cert = client_cert_path,
        # client_key = client_key_path,
        use_ssl = settings.ES_USE_SSL,
        verify_certs = settings.ES_VERIFY_CERTS,
        ssl_assert_hostname = False,
        ssl_show_warn = False,
    )
    return new_os_client


def create_os_indices_client():
    log.info("Creating indices client")
    indices_client = IndicesClient(client=get_os_client())
    return indices_client

def create_os_cat_client():
    log.info("Creating cat client")
    cat_client = CatClient(client=get_os_client())
    return cat_client