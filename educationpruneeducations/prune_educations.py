import logging
from datetime import datetime, timezone

import boto3
import jmespath
from botocore.client import Config
from opensearchpy.exceptions import RequestError

from educationpruneeducations import settings
from educationpruneeducations.opensearch_common import create_os_indices_client, create_os_cat_client

EDUCATIONPLANS_PREFIX_PATTERN = 'educationplans_'

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


def prune_educations():
    log.info('Starting to prune educations, Opensearch-indexes and Minio-files..')
    prune_opensearch_indexes()
    prune_minio_files()


def prune_minio_files():
    prefix_patterns = settings.PRUNE_EDUCATIONS_S3_FILE_PREFIX.split(';')
    log.info(f'Configured prefix_patterns for pruning in Minio: {prefix_patterns}')
    now_datetime = datetime.now(timezone.utc)
    s3_url = settings.S3_URL
    s3_bucket_name = settings.S3_BUCKET_NAME
    aws_access_key = settings.AWS_ACCESS_KEY
    aws_secret_access_key = settings.AWS_SECRET_ACCESS_KEY
    s3_client = boto3.client('s3',
                             endpoint_url=s3_url,
                             aws_access_key_id=aws_access_key,
                             aws_secret_access_key=aws_secret_access_key,
                             config=Config(signature_version='s3v4'),
                             region_name='us-east-1')
    objects = s3_client.list_objects(Bucket=s3_bucket_name)
    all_files_in_bucket = []
    for bucket_obj in objects["Contents"]:
        file_name = bucket_obj['Key']
        file_last_modified = bucket_obj['LastModified']
        file_size = bucket_obj['Size']
        all_files_in_bucket.append({'file_name': file_name,
                                    'file_last_modified': file_last_modified,
                                    'file_size': file_size})
    log.debug(all_files_in_bucket)
    sorted_files_in_bucket = sorted(all_files_in_bucket, key=lambda d: d['file_last_modified'].timestamp(),
                                    reverse=True)
    log.debug('sorted_files_in_bucket: ' + str(sorted_files_in_bucket))
    educationplan_prefix_patterns = get_educationplans_prefixes(sorted_files_in_bucket)

    prefix_patterns.extend(educationplan_prefix_patterns)

    filenames_to_keep = get_filenames_to_keep(prefix_patterns, sorted_files_in_bucket)
    log.info('Files that are youngest and wont be pruned: ' + str(filenames_to_keep))

    deleted_files_count = 0

    for bucketfile in sorted_files_in_bucket:
        file_name = bucketfile['file_name']
        file_last_modified = bucketfile['file_last_modified']

        if file_name.startswith(tuple(prefix_patterns)):

            if not file_name in filenames_to_keep:
                diff = now_datetime - file_last_modified
                diff_in_hours = round(diff.total_seconds() / 3600, 2)
                log.debug(f'File is {diff_in_hours} hours old')

                if diff_in_hours > settings.PRUNE_EDUCATIONS_S3_FILE_AFTER_HOURS:
                    log.info(f'File {file_name} is older than limit and will be deleted '
                             f'(age: {diff_in_hours} hours, limit: {settings.PRUNE_EDUCATIONS_S3_FILE_AFTER_HOURS} hours)')
                    s3_client.delete_object(
                        Bucket=s3_bucket_name,
                        Key=file_name,
                    )
                    deleted_files_count += 1
                else:
                    time_left = settings.PRUNE_EDUCATIONS_S3_FILE_AFTER_HOURS - diff_in_hours
                    log.info(
                        f'File {file_name} matches with prune pattern but is not old enough to prune. Keeping it for {round(time_left, 2)} hours.')

    log.info(f'Deleted {deleted_files_count} files from Minio-bucket.')


def get_filenames_to_keep(prefix_patterns, sorted_files_in_bucket):
    filenames_to_keep = []
    for prefix_pattern in prefix_patterns:
        files_for_pattern = [bucketfile for bucketfile in sorted_files_in_bucket if
                             bucketfile['file_name'].startswith(prefix_pattern)]
        if files_for_pattern:
            filenames_to_keep.append(files_for_pattern[0]['file_name'])

    return filenames_to_keep


def get_educationplans_prefixes(sorted_files_in_bucket):
    educationplan_prefix_patterns = []
    for bucketfile in sorted_files_in_bucket:
        if bucketfile['file_name'].startswith(EDUCATIONPLANS_PREFIX_PATTERN):
            parts = bucketfile['file_name'].split('_')
            prefix_pattern = '_'.join([parts[0], parts[1]])
            educationplan_prefix_patterns.append(prefix_pattern)
    educationplan_prefix_patterns = list(set(educationplan_prefix_patterns))
    return educationplan_prefix_patterns


def prune_opensearch_indexes():
    os_indices_client = create_os_indices_client()
    alias_response = os_indices_client.get(settings.ES_EDUCATIONS_ALIAS)
    indexes_to_keep = alias_response.keys()

    os_cat_client = create_os_cat_client()
    indexes_response = os_cat_client.indices(h='i', params={'timeout': 60})
    all_indexes = indexes_response.split('\n')

    now_datetime = datetime.now()
    deleted_indexes_count = 0
    log.info(f'Found a total of {len(all_indexes)} indexes in Opensearch.')
    log.info(f'Will prune indexes older than {settings.PRUNE_EDUCATIONS_ES_INDEX_AFTER_HOURS} hours, starting with name "{settings.PRUNE_EDUCATIONS_ES_INDEX_PATTERN}".')
    for index_name in all_indexes:
        if index_name.startswith(settings.PRUNE_EDUCATIONS_ES_INDEX_PATTERN) and not index_name in indexes_to_keep:

            index_details = os_indices_client.get(index_name)
            log.debug(index_details)

            index_creation_date = int(jmespath.search(f'"{index_name}".settings.index.creation_date', index_details))

            index_creation_datetime = datetime.fromtimestamp(index_creation_date / 1000.0)

            diff = now_datetime - index_creation_datetime
            diff_in_hours = round(diff.total_seconds() / 3600, 2)
            log.debug(f'Index is {diff_in_hours} hours old')

            if diff_in_hours > settings.PRUNE_EDUCATIONS_ES_INDEX_AFTER_HOURS:
                log.info(f'Index {index_name} is older than limit and will be deleted '
                         f'(age: {diff_in_hours} hours, limit: {settings.PRUNE_EDUCATIONS_ES_INDEX_AFTER_HOURS} hours)')
                try:
                    os_indices_client.delete([index_name])
                    deleted_indexes_count += 1
                except RequestError as exc:
                    log.warning(f"Could not delete index '{index_name}' because of error: {str(exc)}")
            else:
                time_left = settings.PRUNE_EDUCATIONS_ES_INDEX_AFTER_HOURS - diff_in_hours
                log.info(
                    f'Index {index_name} matches with prune pattern but is not old enough to prune. Keeping it for {time_left} hours.')
    log.info(f'Deleted {deleted_indexes_count} indexes from Opensearch.')
